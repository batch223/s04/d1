<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S04: Access Modifiers</title>
</head>
<body>
	<h2>Access Modifiers</h2>

	<h3>Building Object Variable</h3>
	<p><?php var_dump($building);?></p>
	<p><?php //echo $building->name;?></p>

	<h3>Condominium Object Variable</h3>
	<p><?php var_dump($condominium);?></p>
	<p><?php //echo $condominium->name;?></p>

	<h2>Encapsulation</h2>

	<p>The name of the condominium is <?php echo $condominium->getName() ;?></p>

	<?php $condominium->setName('Magic Tower');?>
	<?php $condominium->setName('');?>
	<p>The name of the condominium has been changed to <?php echo $condominium->getName();?> </p>

	<p><?php var_dump($condominium);?></p>

</body>
</html>